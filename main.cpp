/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include <iostream>
#include <stdlib.h>
#include <time.h>

void printInv() {
	std::cout << "Invalid usage. Use -h for help\n";
}

int main(int argc, char** argv) {
	if(argc != 2 && argc != 3) {
		printInv();
		return -1;
	}
	int length = std::atoi(argv[1]);
	if (!length) {
		if(argc == 2) {
			if(argv[1][1] == 'h') {
				std::cout << "Lishy password generator v0.1\n"
								<< "Made by Lishie\n"
								<< "Email: lishie@disroot.org\n\n"
								<< "usage:\n"
								<< "      tripforce [OPTION] LENGTH"
								<< "options:"
								<< "  -h	display help\n"
								<< "  -l	exclude use lowercase letters\n"
								<< "  -u	exclude use uppercase letters\n"
								<< "  -n	exclude use numerals\n"
								<< "  -s	exclude use special characters\n";
				return 0;
			}
			printInv();
			return -1;
		}
		else {
			length = std::atoi(argv[2]);
			if(!length) {
				printInv();
				return -2;
			}
		}
	}
	int exclPools = 0;
	if(argv[1][0] == '-') {
		for(int i = 1; argv[1][i] != '\0'; i++) {
			switch(argv[1][i]) {
			case 'l':
				exclPools += 1;
				break;
			case 'u':
				exclPools += 2;
				break;
			case 'n':
				exclPools += 4;
				break;
			case 's':
				exclPools += 8;
				break;
			default:
				std::cout << "Invalid argument: -" << argv[1][i] << std::endl;
				return -3;
				break;
			}
		}
	}

	if(exclPools > 13) {
		std::cout << "No characters in the pool!\n";
		return -2;
	}

	std::string charPool = "";
	int poolLength = 0;

	if((exclPools & 1) == 0)  {
		charPool += "qwertyuiopasdfghjklzxcvbnm";
		poolLength += 26;
	}
	if((exclPools & 2) == 0) {
		charPool += "QWERTYUIOPASDFGHJKLZXCVBNM";
		poolLength += 26;
	}
	if((exclPools & 4) == 0) {
		charPool += "0123456789";
		poolLength += 10;
	}
	if((exclPools & 8) == 0) {
		charPool += "!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~";
		poolLength += 32;
	}

	srand(time(NULL));
	for(int i = 0; i < length; i++) {
		std::cout << charPool[rand()%poolLength];
	}
	std::cout << std::endl;
	return 0;
}
