all:
	g++ main.cpp -o passgen 
run: all
	./passgen 10
install: all
	cp ./passgen ~/.local/bin/passgen
